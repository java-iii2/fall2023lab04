package debugging.assignment;

public class Company {
    private Employee[] employees;

    public Company(Employee[] employees) {
        this.employees = new Employee[employees.length];
        for(int i = 0; i < employees.length; i++){
            this.employees[i] = new Employee(employees[i]);
        }
    }

    public Company(Company otherComapny){
        this(otherComapny.employees);
    }

    /**
     * Exercise: Fill in the JavaDocs comment to describe what this method is supposed to do!
     * @param name
     * @return
     */
    public boolean doesWorkFor(String name) {
        for (Employee e : employees) {
            if (e.getName().equals(name)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Company)) {
            return false;
        }

        Company other = (Company) o;
        for (int i = 0; i < other.employees.length; i++) {
            if (!other.employees[i].equals(this.employees[i])) {
                return false;
            }
        }

        // temporary print statement to demonstrate how you can see print statements within a unit test.
        System.out.println("hello world");

        return true;
    }

}
